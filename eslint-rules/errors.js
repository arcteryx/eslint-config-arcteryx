module.exports = {
  "rules": {
    "for-direction": "error",
    "no-compare-neg-zero": "error",
    "no-cond-assign": "error",
    "no-console": "error", // Allow console log, but only with careful consideration
    "no-constant-condition": "warn", // Allow, but we should consider erroring, as it's either unnecessary or incomplete code
    "no-control-regex": "error",
    "no-debugger": "warn", // Allow for now, but only with careful consideration
    "no-dupe-args": "error",
    "no-dupe-keys": "error",
    "no-duplicate-case": "error",
    "no-empty": "error",
    "no-empty-character-class": "error",
    "no-ex-assign": "error",
    "no-extra-boolean-cast": "error",
    "no-extra-parens": [ "error", "functions" ],
    "no-extra-semi": "error",
    "no-func-assign": "error",
    "no-inner-declarations": "error",
    "no-invalid-regexp": "error",
    "no-irregular-whitespace": "error",
    "no-obj-calls": "error",
    "no-regex-spaces": "error",
    "no-sparse-arrays": "error",
    "no-template-curly-in-string": "error",
    "no-unexpected-multiline": "error",
    "no-unreachable": "error",
    "no-unsafe-finally": "error",
    "no-unsafe-negation": "error",
    "use-isnan": "error",
    "valid-typeof": [ "error", { "requireStringLiterals": true } ],
  },
};
