module.exports = {
  "rules": {
    "array-bracket-newline": [ "error", { "multiline": true } ],
  	"array-bracket-spacing": [ "error", "always" ],
  	"array-element-newline": [ "error", { "multiline": true } ],
  	"block-spacing": "error",
  	"brace-style": "error",
  	"camelcase": "warn",
  	"capitalized-comments": "error",
  	"comma-dangle": [ "error", "always-multiline" ],
  	"comma-spacing": "error",
  	"comma-style": "error",
  	"computed-property-spacing": [ "error", "never" ],
  	"consistent-this": [ "error", "that" ],
  	"eol-last": "error",
  	"func-call-spacing": "error",
  	"func-name-matching": "error",
  	"func-names": "off", // @TODO: revisit
  	"func-style": [ "warn", "expression" ],
  	"function-paren-newline": [ "error", "multiline" ],

  	/*
  	 * "id-blacklist": "off",
  	 * "id-length": "off",
  	 * "id-match": "off",
  	 */
  	"implicit-arrow-linebreak": "error",
  	"indent": [ "error", 2, { "SwitchCase": 1 } ],
  	"jsx-quotes": [ "error", "prefer-double" ],
  	"key-spacing": [
      "error",
      {
        "beforeColon": false,
        "afterColon": true,
      },
    ],
    "keyword-spacing": "error",
    "line-comment-position": "off",
    "linebreak-style": "error",
    "lines-around-comment": [ "error", { "allowBlockStart": true } ],
    "lines-between-class-members": "error",
    // "max-depth": "off",
    "max-len": [
      "warn",
      {
        "code": 120,
        "comments": 120,
        "ignoreUrls": true,
      },
    ],
    "max-lines": [ "warn", 600 ], // @TODO: bring down
    "max-nested-callbacks": [ "error", 8 ],
    "max-params": [ "warn", 4 ],
    "max-statements": [ "warn", { "max": 50 } ],
    "max-statements-per-line": [ "warn", { "max": 2 } ],
    "multiline-comment-style": "error",
    "multiline-ternary": "off",
    "new-cap": "error",
    "new-parens": "error",
    "newline-per-chained-call": [ "error", { "ignoreChainWithDepth": 1 } ],
    "no-array-constructor": "error",
    "no-bitwise": "error",
    "no-continue": "warn",
    "no-inline-comments": "off",
    "no-lonely-if": "error",
    "no-mixed-operators": "error",
    "no-mixed-spaces-and-tabs": "error",
    "no-multi-assign": "error",
    "no-multiple-empty-lines": [ "error", { "max": 2, "maxEOF": 1 } ],
    "no-negated-condition": "error",
    "no-nested-ternary": "error",
    "no-new-object": "error",
    "no-plusplus": "off",
    // "no-restricted-syntax": "off",
    "no-tabs": "error",
    "no-ternary": "off",
    "no-trailing-spaces": "error",
    "no-underscore-dangle": "warn",
    "no-unneeded-ternary": "error",
    "no-whitespace-before-property": "error",
    "nonblock-statement-body-position": "error",
    "object-curly-newline": "error",
    "object-curly-spacing": [ "error", "always" ],
    "object-property-newline": [ "error", { "allowMultiplePropertiesPerLine": true } ],
    // "one-var": "error", // @TODO: revisit
    "one-var-declaration-per-line": "error",
    "operator-assignment": "off",
    "operator-linebreak": [ "error", "before" ],
    "padded-blocks": [ "error", "never" ],
    "padding-line-between-statements": "off",
    "quote-props": "error",
    "quotes": [ "error", "double" ],
    "require-jsdoc": "off",
    "semi": "error",
    "semi-spacing": "error",
    "semi-style": "error",
    "sort-keys": "off",
    "sort-vars": "off",
    "space-before-blocks": "error",
    "space-before-function-paren": [
      "error",
      {
        "anonymous": "always",
        "named": "never",
        "asyncArrow": "always",
      },
    ],
    "space-in-parens": [ "error", "always" ],
    "space-infix-ops": "error",
    "space-unary-ops": [ "error", { 
      "words": true,
      "overrides": {
        "!": true
      }
     } ],
    "spaced-comment": "error",
    "switch-colon-spacing": "error",
    "template-tag-spacing": "error",
    // "unicode-bom": "off",
    "wrap-regex": "error",
  },
};
