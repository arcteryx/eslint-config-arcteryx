module.exports = {
  "rules": {
    "init-declarations": "off",
    "no-catch-shadow": "off",
    "no-delete-var": "error",
    "no-label-var": "error",
    // "no-restricted-globals": "off"
    "no-shadow": "warn",
    "no-shadow-restricted-names": "error",
    "no-undef": "error",
    "no-undef-init": "error",
    "no-undefined": "error",
    "no-unused-vars": "error",
    "no-use-before-define": "error",
  },
};
