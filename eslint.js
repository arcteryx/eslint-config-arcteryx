module.exports = {
  extends: [
    './eslint-rules/best-practices',
    './eslint-rules/errors',
    './eslint-rules/node',
    // './eslint-rules/style',
    './eslint-rules/variables',
    './eslint-rules/es6',
    './eslint-rules/imports',
  ].map(require.resolve),
  env: {
    es6: true
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaVersion: 2019,
  }
};
