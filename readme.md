# eslint-config-arcteryx

This package provides the Arc'teryx ESLint formatting and styling convention as an extensible shared config.

### Usage

1. Install the package

```sh
	npm install eslint-config-arcteryx
```

2. Create an `.eslintrc` file in your project root:

```js
    {
        "extends": "@arcteryx/eslint-config-arcteryx"
    }
```

3. Create a `prettier.config.js` file in your project root:

```js
module.exports = {
    extends: "./@arcteryx/eslint-config-arcteryx/prettier.js"
};
```

3. [Configure your build process](https://eslint.org/docs/user-guide/configuring) _AND_ [find the correct plugin for your editor](https://eslint.org/docs/user-guide/integrations) to display the output.

### Notes

-   Disallow specific global variables (`no-restricted-globals`) can be overwritten for projects that don't make use of the global `ARCTERYX` object.

### To Do

-   Add ES6 specific rules
